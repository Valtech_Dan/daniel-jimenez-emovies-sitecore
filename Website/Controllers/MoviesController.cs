﻿using EMovies.Models.Movies;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.WebPages;
using Website.Models.Movies;

namespace EMovies.Controllers
{
    public class MoviesController : Controller
    {
        public ActionResult Browse()
        {
            ShoppingBasket basket = (ShoppingBasket)Session["Basket"];

            var dataSource = BrowsePageFunctions.SitecoreDatabase(BrowsePageConstants.HomeItem);

            BrowsePageConstants.SubTitleItems = dataSource;

            if (basket == null)
            {
                return View();
            }
            else
            {
                if (basket.TotalQuantity < 1)
                {
                    ModelState.AddModelError("TotalQuantity", "Please add a quantity of at least 1 movie.");
                }
                return View(basket);
            }
        }

        private ShoppingBasket EmptyBasket()
        {
            ShoppingBasket basket = new ShoppingBasket();
            var dataSource = BrowsePageFunctions.SitecoreDatabase(BrowsePageConstants.MovieNamesItem);
            BrowsePageConstants.MovieTitlesAndPrices = dataSource.Children;
            basket.Movies = new List<Movie>();


            if (BrowsePageConstants.MovieTitlesAndPrices != null)
            {
                foreach (Item movie in BrowsePageConstants.MovieTitlesAndPrices)
                {
                    basket.Movies.Add(new Movie { Name = movie.Fields["Title"].ToString(), Price = movie.Fields["Price"].ToString().AsDecimal()});
                }
            }
            else { throw new Exception(); } //Throw 500 error here. Design pages for 404 and 500.

            return basket;
        }

        public ActionResult MovieList()
        {
            ShoppingBasket basket = (ShoppingBasket)Session["Basket"];
            if (basket == null)
            {
                return View(EmptyBasket());
            }
            else
            {
                return View(basket);
            }
        }

        public ActionResult Update(ShoppingBasket basket)
        {
            Session["Basket"] = basket;
           
            return Redirect(BrowsePageFunctions.GetUrlForItem(BrowsePageConstants.HomeItem));
        }

        public ActionResult MoveToPay()
        {
            ShoppingBasket basket;
            basket = (ShoppingBasket)Session["Basket"];

            if (basket == null || basket.TotalQuantity < 1)
            {
                return Redirect(BrowsePageFunctions.GetUrlForItem(BrowsePageConstants.HomeItem));
            }

            return Redirect(BrowsePageFunctions.GetUrlForItem(BrowsePageConstants.PaymentItem));
        }
    }
}