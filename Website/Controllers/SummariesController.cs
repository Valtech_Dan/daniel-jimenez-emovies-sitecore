﻿using EMovies.Models.Movies;
using System;
using System.Web.Mvc;
using Website.Models.Movies;
using Website.Models.Summaries;

namespace EMovies.Controllers
{
    public class SummariesController : Controller
    {
        public ActionResult Summary()
        {
            PaymentCredentials payment = (PaymentCredentials)Session["newPayment"];
            ShoppingBasket basket = (ShoppingBasket)Session["Basket"];

            if (payment == null || basket == null) 
            {
                return Redirect(BrowsePageFunctions.GetUrlForItem(BrowsePageConstants.HomeItem));
            }

            payment.CardNumber = payment.CardNumber.Substring(Math.Max(0, payment.CardNumber.Length - 4));

            SummaryObject summaryObject = SummaryDetailsFunctions.SummaryFunction(payment, basket);

            Session.Abandon();

            return View(summaryObject);
        }
    }
}