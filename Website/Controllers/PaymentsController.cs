﻿using EMovies.Models.Movies;
using System.Web.Mvc;
using Website.Models.Movies;
using Website.Models.Payments;

namespace EMovies.Controllers
{
    public class PaymentsController : Controller
    {
        private PaymentCredentials EmptyPayment()
        {
            return new PaymentCredentials
            {
                Name = string.Empty,
                Email = string.Empty,
                CardNumber = string.Empty,
                EmailNotification = false,
            };
        }

        public ActionResult Payment()
        {
            ShoppingBasket basket = (ShoppingBasket)Session["Basket"];

            if (basket == null)
            {
                return Redirect(BrowsePageFunctions.GetUrlForItem(BrowsePageConstants.HomeItem));
            }
            else
            {
                PaymentCredentials payment = (PaymentCredentials)Session["newPayment"];

                if (payment == null)
                {
                    return View(EmptyPayment());
                }
                else
                {
                    TryUpdateModel(payment);
                    return View(payment);
                }
            }
        }

        public ActionResult SubmitPayment(PaymentCredentials payment)
        {
            Session["newPayment"] = payment;
            if (!ModelState.IsValid)
            {
                return Redirect(BrowsePageFunctions.GetUrlForItem(PaymentPageItems.HomeItem));
            }
            else
            {
                return Redirect(BrowsePageFunctions.GetUrlForItem(PaymentPageItems.SummaryItem));
            }
        }
    }
}