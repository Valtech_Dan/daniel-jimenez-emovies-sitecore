﻿namespace Website.Models.Payments
{
    public class PaymentPageItems
    {
        public static string HomeItem = "/sitecore/Content/Payment";
        public static string UpdatedPaymentItem = "/sitecore/Content/UpdatedPayment";
        public static string SummaryItem = "/sitecore/Content/Summary";
    }
}