﻿using System.ComponentModel.DataAnnotations;

namespace EMovies.Models.Movies
{
    public class PaymentCredentials
    {
        [Required]
        [RegularExpression(@"^\w{1,40}$", ErrorMessage = "Please keep your name within 40 characters.")]
        public string Name { get; set; }
        
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [RegularExpression(@"^\d{14,18}$", ErrorMessage = "Please enter a valid Card Number.")]
        public string CardNumber { get; set; }

        public bool EmailNotification { get; set; }

        public enum CardIdentifier
        {
            Visa = 1,
            MasterCard = 2,
            AmericanExpress = 3
        }

        [Required]
        [RegularExpression(@"[^0]+", ErrorMessage = "Please enter a valid Card Type.")]
        [Key]
        public CardIdentifier CardType { get; set; }
    }
}