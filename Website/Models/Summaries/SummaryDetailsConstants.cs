﻿using System.Collections.Generic;

namespace EMovies.Models.Movies
{
    public class SummaryObject
    {
        public string Name { get; set; }

        public List<string> MovieNames { get; set; }

        public string Email { get; set; }

        public string CardNumber { get; set; }

        public string CardType { get; set; }
        public bool EmailNotifications { get; set; }
        public List<Movie> Movies { get; set; }

        public IEnumerable<int> Quantities;

        public List<decimal> Prices;
        public decimal Total { get; set; }

    }
}