﻿using EMovies.Models.Movies;
using System.Linq;

namespace Website.Models.Summaries
{
    public class SummaryDetailsFunctions
    {
        public static SummaryObject SummaryFunction(PaymentCredentials payment, ShoppingBasket basket)
        {
            return new SummaryObject
            {
                Name = payment.Name,
                Email = payment.Email,
                CardNumber = payment.CardNumber,
                CardType = payment.CardType.ToString(),
                EmailNotifications = payment.EmailNotification,
                Quantities = basket.Quantities,
                Total = basket.TotalPrice,
                Movies = basket.Movies.Where(movie => movie.Quantity > 0).ToList()
            };
        }
    }
}