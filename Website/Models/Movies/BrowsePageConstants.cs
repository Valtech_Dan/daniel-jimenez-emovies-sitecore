﻿using Sitecore.Data.Items;
using System.Collections.Generic;

namespace Website.Models.Movies
{
    public class BrowsePageConstants
    {
        public static IEnumerable<Item> MovieTitlesAndPrices { get; set; }
        public static Item SubTitleItems { get; set; }

        public static string HomeItem = "/sitecore/Content/Browse";

        public static string MovieNamesItem = "/sitecore/Content/MovieNames";

        public static string PaymentItem = "/sitecore/Content/Payment";
    }
}