﻿namespace EMovies.Models.Movies
{
    public class Movie
    {
        public string Name { get;  set; }

        public string Description { get;  set; }

        public decimal Price { get;  set; }

        public int Quantity { get; set; }

        public decimal Total { get;  set; }
    }
}