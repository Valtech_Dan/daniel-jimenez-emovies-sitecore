﻿using Sitecore.Links;

namespace Website.Models.Movies
{
    public class BrowsePageFunctions
    {
        public static string GetUrlForItem(string Query)
        {
            return LinkManager.GetItemUrl(Sitecore.Context.Database.GetItem(Query));
        }

        public static Sitecore.Data.Items.Item SitecoreDatabase(string Query)
        {
            return Sitecore.Context.Database.GetItem(Query);
        }
    }
}