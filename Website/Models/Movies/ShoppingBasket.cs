﻿using System.Collections.Generic;
using System.Linq;

namespace EMovies.Models.Movies
{
    public class ShoppingBasket
    {
        public List<Movie> Movies { get; set; }
        public decimal TotalPrice
        {
            get { return Movies.Select(movie => movie.Quantity * movie.Price).Sum(); }
        }
        public IEnumerable<int> Quantities
        {
            get { return Movies.Select(movie => movie.Quantity); }
        }
        public IEnumerable<decimal> Prices
        {
            get { return Movies.Select(movie => movie.Price); }
        }
        public int TotalQuantity
        {
            get { return Movies.Select(movie => movie.Quantity).Sum(); }
        }
    }
}